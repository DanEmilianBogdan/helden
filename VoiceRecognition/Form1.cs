﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Net.Mail;
using System.IO;

namespace VoiceRecognition
{
    public partial class Form1 : Form
    {
        int l = 5;
        int k = 0;
        SpeechRecognitionEngine recEngine = new SpeechRecognitionEngine();
        SpeechSynthesizer syn = new SpeechSynthesizer();
        mic microfon = new mic();
        bool lights=false;
        bool doors = false;
        public Form1()
        {
            InitializeComponent();
        }

      
      

        private void Form1_Load(object sender, EventArgs e)
        {
            Choices commands = new Choices();

            string[] words = new string[100];
            string line = "";

            StreamReader file = new StreamReader("E:\\keyWords.txt");

            while (file.Peek() != -1)
            {
                line = file.ReadLine();
                words = line.Split(',');
            }
            file.Close();
            commands.Add(words);
            GrammarBuilder gBuilder = new GrammarBuilder();
            gBuilder.Append(commands);
            Grammar grammar = new Grammar(gBuilder);

            recEngine.LoadGrammarAsync(grammar);
            recEngine.SetInputToDefaultAudioDevice();
            recEngine.SpeechRecognized += RecEngine_SpeechRecognized;
            

       
        }

        private void SendMail()
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("emergency.office95@gmail.com");
            mail.To.Add("marius.aioanei@student.usv.ro");
            mail.To.Add("emilian.dan@student.usv.ro");
            mail.To.Add("tudor.o@yahoo.com");
            mail.To.Add("adrianmihailov123@gmail.com");
            mail.To.Add("mihai.serban@student.usv.ro");
            mail.Subject = "Danger at home - 1";
            mail.Body = "La birtul din gara sunt probleme";

            Attachment attachment;
            attachment = new Attachment("mic.wav");
            mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("emergency.office95@gmail.com", "tryanother_8");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }
        private void RecEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            switch (e.Result.Text)
            {
                case "i need help":
                    microfon.SaveAudio();
                    SendMail();
                    syn.SpeakAsync("You have sent a email for help");
                    break;

                   
                case "safe":
                    syn.SpeakAsyncCancelAll(); 
                    recEngine.RecognizeAsyncStop();
                    pictureBox3.BorderStyle = BorderStyle.Fixed3D;
                    pictureBox2.BorderStyle = BorderStyle.None;
                    microfon.SaveAudio();

                   pictureBox3.Enabled = false;
                    break;
                case "say hello":
                    syn.SpeakAsync("Hello! How are you?");
                    break;


                case "speak text":
                    syn.SpeakAsync(richTextBox1.Text);
                    break;
                case "rape":
                case "help":
                case "hurting me":
                case "if you touch me i wil scream":

                    k++;
                    l -=1;

                    if (k == 5)
                    {
                        syn.SpeakAsync("attention , the doors are closing and the police will be called");
                        System.Threading.Thread.Sleep(1000);
                       // syn.SpeakAsync("Doors are looked" + "And police are on the way");
                        goto case "lock";
                    }

                    richTextBox1.Text += "\nEmergency count is at " + k + " you need " + l + " more keywords before taking an action ";

                    break;

                case "lock":
                    if (lights == false)
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.LOCKED;
                        doors = true;
                    }
                    else
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.LOCKED_LIGHTS;
                        doors = true;
                    }

                    syn.SpeakAsync("Doors are locked");

                    break;

                case "unlock":
                    if (lights == false)
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.OPENED;
                        doors = false;
                    }
                    else
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.OPENED_LIGHTS;
                        doors = false;
                    }
                    syn.SpeakAsync("Doors are unlocked");
                    break;

                case "lights on":
                    if (doors == false)
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.OPENED_LIGHTS;
                        lights = true;
                    }
                    else
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.LOCKED_LIGHTS;
                        lights = true;
                    }
                    syn.SpeakAsync("Lights are on");
                    break;
                case "lights off":
                    if (doors == false)
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.OPENED;
                        lights = false;
                    }
                    else
                    {
                        this.pictureBox1.Image = global::VoiceRecognition.Properties.Resources.LOCKED;
                        lights = false;
                    }
                    syn.SpeakAsync("Lights are off");
                    break;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            recEngine.RecognizeAsync(RecognizeMode.Multiple);
            microfon.StartAudio();
            pictureBox2.Enabled = false;
            pictureBox2.BorderStyle = BorderStyle.Fixed3D;
            pictureBox3.Enabled = true;
            pictureBox3.BorderStyle = BorderStyle.None;
          

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            recEngine.RecognizeAsyncStop();
            pictureBox3.Enabled = false;
            pictureBox2.BorderStyle = BorderStyle.None;
            pictureBox2.Enabled = true;
            pictureBox3.BorderStyle = BorderStyle.Fixed3D;
            microfon.SaveAudio();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

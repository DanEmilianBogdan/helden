﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VoiceRecognition
{
    public class mic
    {
        public mic()
        {
        }
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]

        private static extern int record(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);

        public void StartAudio()
        {
            record("open new Type waveaudio Alias recsound", "", 0, 0);

            record("record recsound", "", 0, 0);
        }

        public void SaveAudio()
        {
            record("save recsound mic.wav", "", 0, 0);

            record("close recsound", "", 0, 0);
        }

       
    }
}
